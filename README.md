# BIBE 2020 Paper submission
Code repository for "CHIMERA: Combining Mechanistic Models and Machine Learning for Personalized Chemotherapy and Surgery Sequencing in Breast Cancer"  by Cristian Axenie and Daria Kurz submitted at IEEE BIBE 2020.

## CHIMERA: Combining Mechanistic Models and Machine Learning for Personalized Chemotherapy and Surgery Sequencing in Breast Cancer

### Abstract:

Mathematical and computational oncology has increased the pace of cancer research towards the advancement of personalized therapy. Serving the pressing need to exploit the large amounts of currently underutilized data, such approaches bring a significant clinical advantage in tailoring the therapy. CHIMERA is a novel system that combines mechanistic modelling and machine learning for personalized chemotherapy and surgery sequencing in breast cancer. It optimizes decision-making in personalized breast cancer therapy by connecting tumor growth behaviour and chemotherapy effects through predictive modelling and learning. We demonstrate the capabilities of CHIMERA in learning simultaneously the tumor growth patterns, across several types of breast cancer, and the pharmacokinetics of a typical breast cancer chemotoxic drug. The learnt functions are subsequently used to predict how to sequence the intervention. We demonstrate the versatility of CHIMERA in learning from tumor growth and pharmacokinetics data to provide robust predictions under two, typically used, chemotherapy protocol hypotheses.

CHIMERA Codebase:

* datasets - the experimental datasets (csv files) and their source, each in separate directories
* models   - codebase to run and reproduce the experiments


Directory structure:

models/CHIMERA/.

* create_init_network.m       - init CHIMERA network (SOM + HL)
* error_std.m                 - error std calculation function
* chimera_core.m              - main script to run CHIMERA
* model_rmse.m                - RMSE calculation function 
* model_sse.m                 - SSE calculation function
* parametrize_learning_law.m  - function to parametrize CHIMERA learning
* present_tuning_curves.m     - function to visualize CHIMERA SOM tuning curves
* randnum_gen.m               - weight initialization function
* tumor_growth_model_fit.m    - function implementing ODE models
* tumor_growth_models_eval.m  - main evaluation on CHIMERA runtime
* visualize_results.m         - visualize CHIMERA output and internals
* visualize_runtime.m         - visualize CHIMERA runtime


Usage: 

* models/CHIMERA/chimera_core.m - main function that runs CHIMERA and generates the runtime output file (mat file)
* models/CHIMERA/tumor_growth_models_eval.m - evaluation and plotting function reading the CHIMERA runtime output file
* models/CHIMERA/chemo_drug_concentration_learning_eval.m - evaluation of CHIMERA pharmacokinetics learning runtime
